# I Ronja Røverdatter får Ronja beskjed om å passe
# seg for elva. Men man kan jo ikke passe seg for
# elva midt i skogen. Derfor passet hun seg for elva
# VED elva, eller heller ved å hoppe over den.
# Slik er det med global. Du må forstå, men du må unngå.

# Å definere en variabel som 'global' på denne måten
# er ikke det samme som skop, dette gjør at en kan endre
# på en global variabel innefra en funksjon.
def en_helt_vanlig_funksjon(var):
    global variabel # Dette magiske ordet er et frikort
    # Kommenter ut linjen over og se hva som skjer!
    variabel = var * 10
    
    
variabel = 10
print(f'Her er variabel {variabel}')
en_helt_vanlig_funksjon(variabel)
print(f'Nå er variabel {variabel}')

# bruk av global er noe vi ikke liker.
# Ikke gjør dette hjemme. Eller på eksamen.
# Eller på hybelen. Eller hos kjæresten.